------------------------ How to Hash input word ------------------------
hash_obj = hashlib.sha256()
        hash_obj.update(message_for_encript.encode('utf_8'))
        return hash_obj.hexdigest()

------------------------ Call user function ------------------------
getattr(self,'test')(call_fun)
------------------------ Change Data function ------------------------

    def ChangeData(self,data):
        #data = пользовательское назавание данных

        self.open_data_file()

        dirt_data = self.find_by_lable(data)

        if not dirt_data['status']:
            self.close_data_file()
            while not dirt_data['status']:
                self.print_error(self._conf[__name__][__name__ + '_error_message']['Data_not_found_message'][self._language] % (data))
                self.open_data_file()
                data = self._user_input_form(self._conf[__name__][__name__ + '_error_message']['Try_again_message'][self._language])
                dirt_data = self.find_by_lable(data)
                self.close_data_file()
        fetch_data = self.fetch_assocc(dirt_data['data'])
        clear_data = self._decrypt_data(fetch_data)
        del(fetch_data)

        print(clear_data)
        self._show_data_table(clear_data)

    def ResetPassword(self,data=None):

        password_confirm = False

        if self.check_auth(data):
            password_confirm = True
        else:
            if self.repeat_password(self.hash(self._secrect_user_key)):
                password_confirm = True

        if password_confirm:
            first_password = self._user_input_form(self._conf[__name__][__name__ + '_messages']['Create_password_message'][self._language],is_pass=True)
            two_password = self._user_input_form(self._conf[__name__][__name__ + '_messages']['Confirm_password_message'][self._language] % (self._conf[__name__]['confirm_']['password'][self._language]),is_pass=True)
            if first_password == two_password:
                return {'status': self._reset_password(first_password), 'data': None, 'render': False}
            else:
                if self.repeat_password(self.double_hash(first_password)):
                    return {'status': self._reset_password(first_password), 'data': None, 'render': False}

-------------- conf.json "ChangeData menu setting" ---------------------
         {
                "action": "ChangeData",
                "name": {
                    "eng": "Change data",
                    "rus": "Изменить данные"
                }
            },

