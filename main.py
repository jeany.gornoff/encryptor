# try:
import hashlib
import os
import screeninfo
import autoloader
from ast import literal_eval
from Encriptor_Base import Encriptor_Base
from Enc import Enc
from UI_Base import UI_Base
# except ModuleNotFoundError:
#     autoloader.update()


def restart():
    os.system('python main.py')

try:
    if __name__ == '__main__':
        enc = Enc()
        if enc.init_system_check() == 'reload':
            restart()
        else:
            enc.run()
        # enc.test()
except NameError:
    print('\033[31m' + '! Modules not found !')


