import os
import time

from getpass import getpass as inpass
from Config_Controller import Config_Controller
from colors import colors
from prettytable import PrettyTable

class UI_Base(object):

    def __init__(self,lan = None ):
        self._conf = Config_Controller().getConf()
        self.colors = colors()
        self._table = PrettyTable()
        self._table.horizontal_char = '~'
        self._language = lan if lan != None else self._conf['default_language']



    def render_data(self,data):
        self._table.min_table_width = self.get_max_wight()
        self._table.field_names = self._conf[__name__]['table_header'][self._language]
        for i in data:
            self._table.add_row([i[0], i[1], i[2]])

        print(self._table)

        time.sleep(0.5)
        input(self._conf[__name__][__name__ + '_message']['Continue_message'][self._language])
        self._table.clear()
        return True

    def show_main_menu(self,action_name = None):
        self.clear_screen()
        print(self._conf[__name__]['title'])
        menu_point_iterator = 1
        for point_menu in self._conf[__name__]['options']:
            print(self._conf[__name__]['prefix'], (0 if point_menu['name'][self._language] in ('Exit', 'Выход') else str(menu_point_iterator)), '-', point_menu['name'][self._language])
            menu_point_iterator += 1
        print(self._conf[__name__]['footer'])

    def show_auth_message(self):
        print(self.colors.fg.lightblue + self._conf[__name__]['messages']['hello_message'][self._language])
        print(self.colors.reset + self._conf['delimeters']['delimeter_info'])
        return (self._conf[__name__]['messages']['enter_password_prefix'][self._language])

    def get_password_placehoder(self):
        return (self._conf[__name__]['messages']['enter_password_prefix'])


    def set_deffault_font_color(self):
        return self.colors.reset

    def exit(self,data = None):
        print(self._conf[__name__][__name__ + '_message']['Exit_message'][self._language])
        return True

    @staticmethod
    def get_max_wight():
        rows, columns = os.popen('stty size', 'r').read().split()
        return int(columns)

    @staticmethod
    def clear_screen():
        if os.name == 'posix':
            os.system('clear')
        else:
            os.system('cls')

    def get_user_input(self,title=None, color=None,is_pass=False):
        if title != None:
            title = title
            title += color if color != None else ''

            return input(title) if is_pass == False else inpass(title)
        else:
            print(self.colors.fg.red + "Error, title is empty!")
            return False

    def get_user_choice(self, lable=None):
        title = self._conf[__name__]['prefix']
        title += lable if lable != None else self._conf[__name__]['messages']['user_choice_prefix'][self._language]
        try:
            return input(title)
        except ValueError:
            print(self.colors.fg.red + self._conf[__name__][__name__ + '_error_message']['Incorrect_input_message'][self._language])
            print(self.colors.reset + self._conf['delimeters']['delimeter_error'])
            self.get_user_choice(lable)


    def get_action(self,user_choice):
        try:
            return self._conf[__name__]['options'][int(user_choice) - 1]['action']
        except IndexError:
            print(self.colors.fg.red + self._conf[__name__][__name__ + '_error_message']['Unknown_option_message'][self._language])
            print(self.colors.reset + self._conf['delimeters']['delimeter_error'])
            return None
        except TypeError:
            print(self.colors.fg.red + self._conf[__name__][__name__ + '_error_message']['Unknown_option_message'][self._language])
            print(self.colors.reset + self._conf['delimeters']['delimeter_error'])
            return None
        except ValueError:
            print(self.colors.fg.red + self._conf[__name__][__name__ + '_error_message']['Unknown_option_message'][self._language])
            print(self.colors.reset + self._conf['delimeters']['delimeter_error'])
            return None


#---------------------------- Action Menu Func --------------------------------------------------------

    def ChangeData(self,action_name):
        self.clear_screen()
        print(self._conf[__name__]['action_header'] % (action_name))

        user_lable_data = self.get_user_input(self._conf[__name__][__name__ + '_message']['Add_data_lable_message'][self._language])
        return user_lable_data


    def ResetPassword(self,action_name):
        self.clear_screen()
        print(self._conf[__name__]['action_header'] % (action_name))

        confrm_password = self.get_user_input(self._conf[__name__][__name__ + '_message']['Reset_password_message'][self._language],is_pass=True)
        return confrm_password




    def EncryptData(self, action_name):
        self.clear_screen()
        print(self._conf[__name__]['action_header'] % (action_name))
        lable = self.get_user_input(self._conf[__name__][__name__ + '_message']['Add_data_lable_message'][self._language])
        login = self.get_user_input(self._conf[__name__][__name__ + '_message']['Add_data_login_message'][self._language])
        password = self.get_user_input(self._conf[__name__][__name__ + '_message']['Add_data_password_message'][self._language],is_pass=True)
        if lable and login and password:
            return [lable, login, password]


    def DecryptData(self,action_name):
        self.clear_screen()
        print(self._conf[__name__]['action_header'] % (action_name))
        print(self._conf[__name__]['prefix'] + self._conf[__name__]['data_select'][self._language][0] + self._conf[__name__]['prefix'] + self._conf[__name__]['data_select'][self._language][1])

        user_choice = self.get_user_choice(self._conf[__name__][__name__ + '_message']['Select_option_message'][self._language])
        while True:
            if user_choice == '1':
                return {'mode': 'single', 'lable': self.get_user_choice(self._conf[__name__][__name__ + '_message']['Add_data_lable_message'][self._language])}
            elif user_choice == '2':
                return {'mode': 'all'}
            else:
                print(self.colors.fg.red + self._conf[__name__][__name__ + '_error_message']['Unknown_option_message'][self._language])
                print(self.colors.reset + self._conf['delimeters']['delimeter_error'])
                user_choice = self.get_user_choice()





