import os

try:
    from getpass import getpass as inpass
    from Encriptor_Base import Encriptor_Base
    from UI_Base import UI_Base as UI
    from colors import colors
    from Config_Controller import Config_Controller
except FileNotFoundError:
    print('Fatal error')


class Enc(Encriptor_Base, UI):

    def __init__(self, language = None):
        self._enc_base = Encriptor_Base()
        self._ui = UI()
        self._config_controller = Config_Controller()
        self._conf = self._config_controller.getConf()
        self._colors = colors
        self._user_login = False

    def input_password_form(self, color=None, prefix=None):
        password_place_holder = self._ui.get_password_placehoder() if prefix == None else prefix

        return inpass(password_place_holder + color if color != None else '')

    def show_main_menu(self):
        self._ui.show_main_menu()


    def init_check(self):
        return self._enc_base.init_system_check()


    def auth(self):
        password_place_holder = self._ui.show_auth_message()
        user_password = inpass(password_place_holder)
        if self._enc_base.check_auth(user_password):
            self._ui.clear_screen()
            self._user_login = True
            return True
        else:
            user_password = self.input_password_form(self._colors.fg.red, self._conf[__name__][__name__ + '_error_message']['Auth_denied_message'][self._conf['default_language']])
            if self._enc_base.check_auth(user_password):
                UI.clear_screen()
                self._user_login = True
                return True
            else:
                user_password = self.input_password_form(self._colors.fg.red,self._conf[__name__][__name__ + '_error_message']['Auth_denied_message'][self._conf['default_language']])
                while True:
                    if self._enc_base.check_auth(user_password):
                        UI.clear_screen()
                        self._user_login = True
                        return True
                    else:
                        user_password = self.input_password_form(self._colors.fg.red, self._conf[__name__][__name__ + '_error_message']['Auth_denied_message'][self._conf['default_language']])



    def run(self):
        if(self._user_login):
            self._ui.show_main_menu()
            user_choice = self._ui.get_user_choice()
            action = self._ui.get_action(user_choice)
            if action == None:
                while True:
                    user_choice = self._ui.get_user_choice()
                    action = self._ui.get_action(user_choice)
                    if action != None:
                        data = getattr(self._ui, action)(action)
                        result = getattr(self._enc_base, action)(data)
                        if result['render']:
                            self._ui.render_data(result['data'])
                        self.run()

            else:
                data = getattr(self._ui, action)(action)
                result = getattr(self._enc_base, action)(data)
                if result['render']:
                    self._ui.render_data(result['data'])
            self.run()
        else:
            self.auth()
            self.run()

    def test(self):
        self._enc_base.test_table()