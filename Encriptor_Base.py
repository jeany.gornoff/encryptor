import hashlib
import os
import random
import sys
import time
from pathlib import Path

import prettytable
import UI_Base

from getpass import getpass as inpass
from ast import literal_eval
from Config_Controller import Config_Controller as CC
from colors import colors


class Encriptor_Base(object):


    MIN_CHARACTER_RANGE = 32
    MAX_CHARACTER_RANGE = 126
    WORKING_CHARACTER_RANGE_LENGHT = 93
    ASCII_COUNT_CHARACTER = 127

    def __init__(self,language = None):
        self._config_controller = CC()
        self._conf = self._config_controller.getConf()
        self._end_hash_file_name = self._conf[__name__]['end_hash_file_name']
        self._colors = colors()
        self._language = language if language != None else self._conf['default_language']




    def execute_action(self,action_name,data = None):
        return getattr(self,action_name)(data if data != None else None)


#------------------------------------------ Authorization --------------------------------------------------------------

    def check_auth(self,user_password):
        if os.path.isfile(self._conf[__name__]['end_hash_file_path'] + self._end_hash_file_name):
            s_key = user_password
            user_password = self.double_hash(user_password)
            self._file = open(self._conf[__name__]['end_hash_file_path'] + self._end_hash_file_name, 'r+')
            file_hash = self._file.readline()
            if(file_hash == user_password and file_hash != ''):
                self._secrect_user_key = self.hash(s_key)
                print(self._colors.fg.green + self._conf[__name__][__name__ + '_success_message']['Access_success_message'][self._language])
                print(self._colors.reset + self._conf['delimeters']['delimeter_success'])
                self._file.close()
                time.sleep(1)
                return True

            elif file_hash == '':
                print(self._colors.fg.red + self._conf[__name__][__name__ + '_error_message']['File_empty_message'][self._language] % (
                    self._end_hash_file_name))
                print(self._colors.reset + self._conf['delimeters']['delimeter_error'])
                return self.init()

            else:
                return False

        else:
            print(self._colors.fg.red + self._conf[__name__][__name__ + '_error_message']['FileNotFoundError_message'][self._language] % (self._end_hash_file_name))
            time.sleep(1)
            return self.init()



    def double_hash(self,user_password):
        hash_one = hashlib.sha256(user_password.encode('utf-8')).hexdigest()
        hash_two = hashlib.sha256(hash_one.encode('utf-8')).hexdigest()
        return hash_two


    def hash(self,user_password):
        return hashlib.sha256(user_password.encode('utf-8')).hexdigest()

    def init_password(self):
        user_password = s_key = inpass(self._conf[__name__][__name__ + '_messages']['Create_password_message'][self._language])
        enc_user_password = self.double_hash(user_password)

        confirm_user_password = inpass(self._conf[__name__][__name__ + '_messages']['Confirm_password_message'][self._language] % (self._conf[__name__]['confirm_']['password'][self._language]))
        enc_confirm_user_password = self.double_hash(confirm_user_password)

        if enc_user_password == enc_confirm_user_password:
            self._secrect_user_key = self.hash(s_key)
            return enc_user_password
        else:
            if self.repeat_password(enc_user_password, self._conf[__name__][__name__ + '_error_message']['Confirm_denied_message'][self._language]):
                self._secrect_user_key = self.hash(s_key)
                return True

    def init(self):
        if self.create_file():
            user_password = self.init_password()
            if user_password:
                print(self._colors.fg.green + self._conf[__name__][__name__ + '_success_message']['Init_success_message'][self._language])
                time.sleep(1)
                self._file.write(user_password)
                self._file.close()
                self._colors.reset_color()
                return True

        return False

# ------------------------------------------ Init System Func ----------------------------------------------------------



    def create_run_file(self):
        os.system('touch %s' % (self._conf[__name__]['run_file_name']))
        with open(self._conf[__name__]['run_file_name'],'w') as run_file:
            run_file.write(self._conf[__name__]['initialization_run_file_text'] % (self._conf[__name__]['core_dir']))
        os.system('chmod +x %s' % (self._conf[__name__]['run_file_name']))
        os.system('mv %s ../' % (self._conf[__name__]['run_file_name']))



    def init_system_check(self):
        if self._conf[__name__]['core_dir'] == '':
            current_dir = Path(__file__).resolve().parent
            current_dir = str(current_dir) + '/'
            current_dir = current_dir.replace('\\', '/')
            self._config_controller.change_Config(__name__, 'core_dir', current_dir)
            return 'reload'
        elif not os.path.isfile('../%s' % (self._conf[__name__]['run_file_name'])):
            self.create_run_file()
            return 'reload'


        if self._conf[__name__]['end_hash_file_path'] != '':
            return True
        else:
            self._config_controller.change_Config(__name__, 'end_hash_file_path', self._conf[__name__]['core_dir'])
            self._config_controller.change_Config(__name__, 'data_file_path', self._conf[__name__]['core_dir'])
            return 'reload'


    def repeat_password(self,enc_password, prefix_message = None):

        if not prefix_message:
            repeated_password = inpass(self._colors.fg.red +  self._conf[__name__][__name__ + '_error_message']['Mismatch_password_message'][self._language])
            print(self._colors.reset + self._conf['delimeters']['delimeter_error'])
        else:
            repeated_password = inpass(self._colors.fg.red + prefix_message)
            print(self._colors.reset + self._conf['delimeters']['delimeter_error'])

        repeated_password = self.double_hash(repeated_password)

        if enc_password == repeated_password:
            print(self._colors.fg.green + self._conf[__name__][__name__ + '_success_message']['Confirm_access_message'][self._language])
            print(self._colors.reset + self._conf['delimeters']['delimeter_success'])
            return enc_password
        else:
            while enc_password != repeated_password:
                repeated_password = inpass(self._colors.fg.red + prefix_message if prefix_message != None else self._conf[__name__][__name__ + '_error_message']['Mismatch_password_message'][self._language])
                confirm_password = self.double_hash(repeated_password)
                if enc_password == confirm_password:
                    print(self._colors.fg.green + self._conf[__name__][__name__ + '_success_message']['Confirm_access_message'][self._language])
                    print(self._colors.reset + self._conf['delimeters']['delimeter_success'])
                    return confirm_password

    def calculate_hex(self,hex_number):
        return  literal_eval('0x'+hex_number)


    def enc_salt_count(self,salt_count):
        salt_ = str(salt_count)
        key = self._secrect_user_key[-2:]

        enc_salt_cont = ''

        for i in range(len(salt_)):
            s_c_in_ASCII_number = (ord(salt_[i]) + ord(key[i])) - self.WORKING_CHARACTER_RANGE_LENGHT if ord(salt_[i]) + ord(key[i]) >= self.ASCII_COUNT_CHARACTER else ord(salt_[i]) + ord(key[i])
            s_c_in_ASCII_char = chr(s_c_in_ASCII_number)
            enc_salt_cont += s_c_in_ASCII_char

        return enc_salt_cont

    def get_salt(self,len_salt):
        salt_string = ''
        for i in range(len_salt):
            salt_string += chr(random.randint(self.MIN_CHARACTER_RANGE, self.MAX_CHARACTER_RANGE))

        return [salt_string,len(salt_string)]

    def add_salt(self,data):


        salt_1 = self.get_salt(len(self._secrect_user_key) - len(data[1]) - 2)
        salt_2 = self.get_salt(len(self._secrect_user_key) - len(data[2]) - 2)

        salt_1_count = self.enc_salt_count(salt_1[1])
        salt_2_count = self.enc_salt_count(salt_2[1])

        data[1] += salt_1[0] + salt_1_count
        data[2] += salt_2[0] + salt_2_count

        return data

    def __open_file(self,file_name,mode):
        try:
            if self._conf[__name__]['end_hash_file_path'] != '':
                print(self._conf[__name__]['end_hash_file_path'] + f'/{file_name}')
                self._file = open(self._conf[__name__]['end_hash_file_path'] + f'/{file_name}',mode)
            else:
                self._file = open(file_name, mode)
            return True
        except FileExistsError:
            print(self._colors.fg.red + self._conf[__name__][__name__ + '_error_message']['FileExistsError_message'][self._language])
        except FileNotFoundError:
            print(self._colors.fg.red + self._conf[__name__][__name__ + '_error_message']['FileNotFoundError_message'][self._language])


    def __check_file_in_path(self,file_name):
        try:
            if os.path.isfile(file_name):
                return True
            return False
        except FileNotFoundError:
            print(self._colors.fg.red + self._conf[__name__][__name__ + '_error_message']['FileNotFoundError_message'][self._language])
            return False


    def _reset_password(self,new_password,is_hash = False):
        if self.__check_file_in_path(self._conf[__name__]['end_hash_file_path'] + self._end_hash_file_name):
            self._secrect_user_key = new_password if is_hash == True else self.hash(new_password)
            self.__open_file(self._end_hash_file_name,'w')
            password_hash = self.double_hash(new_password) if is_hash == False else new_password
            if self._file.write(password_hash):
                print(self._colors.fg.green + self._conf[__name__][__name__ + '_success_message']['Reset_password_success_message'][self._language])
                print(self._colors.reset + self._conf['delimeters']['delimeter_success'])
                self._file.close()
                time.sleep(0.8)
            else:
                return False
        else:
            print(self._colors.fg.red + self._conf[__name__][__name__ + '_error_message']['FileExists_error_message'][self._language])
            print(self._colors.reset + self._conf['delimeters']['delimeter_error'])
            time.sleep(0.5)
            return False





    def _decrypt_data(self,enc_data):
        lable = enc_data[0]
        enc_data = [enc_data[1], enc_data[2]]
        data_with_not_salt = self.clear_salt(enc_data)
        if data_with_not_salt:
            clear_data = []
            key = self._secrect_user_key
            for element_with_data_in_massive in data_with_not_salt:
                clear_data_string =''
                key_iterator = 0
                for i in range(len(element_with_data_in_massive)):
                    alfa = 0
                    if element_with_data_in_massive[i] == "\\":
                        if element_with_data_in_massive[i] == '\\' and element_with_data_in_massive[i+1] == 'x':
                            j = i+1
                            hex_code = element_with_data_in_massive[j+1]+ element_with_data_in_massive[j+2]
                            hex_value_in_dic = self.calculate_hex(hex_code)
                            alfa = hex_value_in_dic - ord(key[key_iterator])
                            i += 3
                            key_iterator = i - key_iterator
                    else:
                        alfa = ord(element_with_data_in_massive[i]) - ord(key[key_iterator])

                    alfa += self.WORKING_CHARACTER_RANGE_LENGHT if alfa <= self.MIN_CHARACTER_RANGE else 0
                    clear_data_string += chr(alfa)
                    key_iterator += 1
                clear_data += [clear_data_string]
            clear_data.insert(0,lable)
            return clear_data
        else:
            return False

    def get_salt_count(self,string,data=None):
        try:
            if data == None:
                count_salt_string = string[-2:]
                key = self._secrect_user_key[-2:]
                count = ''

                for i in range(len(count_salt_string)):
                    s_c_dif = ord(count_salt_string[i]) - ord(key[i])
                    s_c_dif += self.MIN_CHARACTER_RANGE if s_c_dif <= self.MIN_CHARACTER_RANGE else 0
                    s_c_dif = chr(s_c_dif)
                    count += s_c_dif
                count = int(count) + 2
                return count

        except ValueError:
            print(self._colors.fg.red + self._conf[__name__][__name__ + '_error_message']["FATAL_DECRYPT_ERROR_message"][self._language])
            input(self._conf[__name__][__name__ + '_messages']['Return_to_menu_message'][self._language])
            print(self._colors.reset + self._conf['delimeters']['delimeter_error'])
            return False

    def exit(self,data = None):
        sys.exit()


    def get_lable(self,data):
        dirt_lable_data = data[0]
        return dirt_lable_data.split(self._conf[__name__]['encrypt_mask_lable_char'])[1]


    def _user_input_form(self,text,prefix=None,color=None,is_pass=False):
        form_text = prefix if prefix != None else ''
        form_text += text
        form_text += color if color != None else ''
        return input(form_text) if is_pass == False else inpass(text)


    def fetch_assocc(self,data):

        #Нарезаем строку с данными по ">|<" такому делиметру
        first_sifting = data.split(self._conf[__name__]['encrypt_mask_data_char']['data_delimeter'])
        lable = self.get_lable(first_sifting)

        second_data = []
        #Очищаем данные от пробелов
        for i in first_sifting:
            # в second_data буду данные без пробелов,но с заголовком(lable) и переводом на новую строку
            second_data += [i.replace(' ', '')]

        del first_sifting

        #Очищаем данные от заговока(lable) и убераем елемен перевода на новую строку
        second_data_2 = []
        for i in second_data:
            if i[:3] == self._conf[__name__]['encrypt_mask_data_char']['open']:
                second_data_2 += [i]

        del second_data

        clear_data = []
        #Окончательно чистим данные,убираем разделители начала данных,окончание,и флаг где идут данные
        for i in second_data_2:
            #Убираем "#${" - начало данных по маске
            delete_open_char = i.replace(self._conf[__name__]['encrypt_mask_data_char']['open'],'')
            # Убираем "}#$" - окончание данных по маске
            delete_close_char = delete_open_char.replace(self._conf[__name__]['encrypt_mask_data_char']['close'],'')
            # Убираем фраг начала кодированных данных "?"
            delete_open_data_char = delete_close_char[1:]
            # Кидаем чистые данные в новый массив
            clear_data += [delete_open_data_char]

        del second_data_2

        clear_data.insert(0,lable)

        return clear_data


    def clear_salt(self,data):

        offset_1 = self.get_salt_count(data[0])
        offset_2 = self.get_salt_count(data[1])
        if offset_1 and offset_2 != False:
            data[0] = data[0][:-(offset_1)]
            data[1] = data[1][:-(offset_2)]
            return data
        else:
            return False

    def _show_data_table(self,data):
        table = prettytable.PrettyTable()
        table.min_table_width = UI_Base.UI_Base.get_max_wight()
        table.field_names = self._conf['UI_Base']['table_header'][self._language]
        table.add_row([data[0], data[1], data[2]])

        print(table)


    def get_all_data(self):
        self.open_data_file('r')

        enc_data = []
        for line in self._data_file:
            if line != '':
                enc_data += [line]

        self.close_data_file()
        return enc_data

    def check_lable(self,data_string, lable):

        data_lable = data_string.split(self._conf[__name__]['encrypt_mask_lable_char'])

        for i in data_lable:
            if i == lable:
                return True

        return False

    def print_error(self, text, color=None):
        print(color if color != None else self._colors.fg.red + text )
        print(self._colors.reset + self._conf['delimeters']['delimeter_error'])

    def find_by_lable(self,lable):

        if lable == '':
            print(self._conf[__name__][__name__ + '_error_message']['Empty_lable_message'][self._language])
            return {'status': False, 'data':None}

        cycle = True
        while cycle:
            row = self._data_file.readline()
            if self.check_lable(row,lable):
                cycle = False
                return {'status': True, 'data': row}
            elif row == '':
                cycle = False
                return {'status': False, 'data': None}



    def _get_user_change_menu(self):
        for i in self._conf['UI_Base']['change_data_select'][self._language]:
            print(i)



    def create_file(self):
        if not os.path.isfile(self._conf[__name__]['end_hash_file_path'] + self._end_hash_file_name):
            user_choise = input(self._colors.reset + self._conf[__name__][__name__ + '_messages']['Create_file_message'][self._language] % (self._end_hash_file_name) + ' ')

            while user_choise.lower() != 'y' or user_choise.lower() != 'n':
                if user_choise.lower() == 'y':

                    self._file = open(self._conf[__name__]['end_hash_file_path'] + self._end_hash_file_name, 'w+')

                    print(self._colors.fg.green + self._conf[__name__][__name__ + '_success_message']['file_create'][self._language] % (self._end_hash_file_name))
                    print(self._colors.reset + self._conf['delimeters']['delimeter_success'])
                    return True
                elif user_choise.lower() == 'n':
                    print(self._conf[__name__][__name__ + '_messages']['Goodbye_message'][self._language])
                    exit()
                else:
                    user_choise = input(self._conf[__name__][__name__ + '_error_message']['Unknown_option_message'][self._language] + ' ')
        else:
            return True

    def open_data_file(self,mod=None):
        if os.path.isfile(self._conf[__name__]['data_file_path'] + self._conf[__name__]['data_file_name']):
            data_file_full_name = self._conf[__name__]['data_file_path'] + self._conf[__name__]['data_file_name']
            self._data_file = open(data_file_full_name, mod if mod != None else 'r+')
            return True
        else:
            self._data_file = open(self._conf[__name__]['data_file_path'] + self._conf[__name__]['data_file_name'],'w+')
            return True


    def close_data_file(self):
        self._data_file.close()
        return True


    def wright_data(self,data):
        self.open_data_file('a+')

        data_string = self._conf[__name__]['encrypt_mask'] % (data[0], data[1], data[2])
        if self._data_file.write(data_string):
            return True

        self.close_data_file()




#--------------------------------------------- Action Menu Func --------------------------------------------------------


    def DecryptData(self,data):

        self.open_data_file('r')

        if data['mode'] == 'single':
            enc_data = self.find_by_lable(data['lable'])
            if enc_data['status']:
                enc_data = self.fetch_assocc(enc_data['data'])
                decrypt_data = self._decrypt_data(enc_data)
                if decrypt_data:
                    self.close_data_file()
                    return {'status': True, 'data': [decrypt_data], 'render': True}
                else:
                    return {'status': False, 'data': None, 'render': False}
            else:
                return {'status': False, 'data': None, 'render': False}
        else:
            all_data = self.get_all_data()
            fetch_data = []
            for data in all_data:
                fetch_data += [self.fetch_assocc(data)]

            decrypt_data = []
            for data in fetch_data:
                decrypt_data += [self._decrypt_data(data)]

            if len(decrypt_data) != 0:
                self.close_data_file()
                return {'status': True, 'data': decrypt_data, 'render': True}
            else:
                print(self._colors.fg.lightblue + self._conf[__name__][__name__ + '_error_message']['Data_empty_message'][self._language] % (self._conf[__name__]['data_file_name']))
                print(self._colors.reset + self._conf['delimeters']['delimeter_info'])
                time.sleep(1)
                self.close_data_file()
                return {'status': False, 'data': None, 'render': False}




    def EncryptData(self,data):

        data_login, data_password = data[1], data[2]

        enc_data_result = [data[0]]

        enc_login_string = ''
        enc_password_string = ''

        for i in range(len(data_login)):
            sym_in_data = data_login[i]
            sym_in_key = self._secrect_user_key[i]
            enc_int_sym = ord(sym_in_data) + ord(sym_in_key)
            if enc_int_sym >= self.ASCII_COUNT_CHARACTER:
                enc_int_sym = enc_int_sym - self.WORKING_CHARACTER_RANGE_LENGHT
                enc_login_string += chr(enc_int_sym)
            else:
                enc_login_string += chr(enc_int_sym)


        for i in range(len(data_password)):
            sym_in_data = data_password[i]
            sym_in_key = self._secrect_user_key[i]
            enc_int_sym = ord(sym_in_data) + ord(sym_in_key)
            if enc_int_sym >= self.ASCII_COUNT_CHARACTER:
                enc_int_sym = enc_int_sym - self.WORKING_CHARACTER_RANGE_LENGHT
                enc_password_string += chr(enc_int_sym)
            else:
                enc_password_string += chr(enc_int_sym)

        enc_data_result += [enc_login_string.rstrip()]
        enc_data_result += [enc_password_string.rstrip()]

        enc_data_result = self.add_salt(enc_data_result)

        if self.wright_data(enc_data_result):
            print(self._colors.fg.green + self._conf[__name__][__name__ + '_success_message']['Data_added_message'][self._language])
            print(self._colors.reset + self._conf['delimeters']['delimeter_success'])
            time.sleep(1)
            return {'status': True, 'data': None, 'render': False}
        else:
            print(self._colors.fg.red + self._conf[__name__][__name__ + '_error_message']['Data_not_wright_message'][self._language])
            print(self._colors.reset + self._conf['delimeters']['delimeter_success'])
            return {'status': False, 'data': None, 'render': False}


