import os

import json

from pathlib import Path

class Config_Controller:

    _config_file_name = 'conf.json'
    _os_name = os.name
    _current_dir = str(Path(__file__).resolve().parent)
    _current_dir += '\\' if _os_name == 'nt' else '/'

    def __init__(self):
        with open(self._current_dir + self._config_file_name, 'r',encoding='utf-8') as file:
            self._config = json.load(file)

    def getConf(self):
        return self._config

    def change_Config(self,model_name, key_name, value):

        self._config[model_name][key_name] = value

        self.__rewrite_file()


    def __rewrite_file(self):
        with open(self._current_dir + self._config_file_name, 'w') as f:
            f.write(json.dumps(self._config,sort_keys = True, indent = 4))
